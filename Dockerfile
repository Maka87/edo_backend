FROM mhart/alpine-node:14
ENV TZ=Asia/Yekaterinburg
	
WORKDIR /src

RUN apk --update add \
tzdata \
&& cp /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN apk --update add \
wkhtmltopdf freetype freetype-dev fontconfig fontconfig-dev \
font-croscore ttf-linux-libertine ttf-liberation

COPY . .

EXPOSE 5600

RUN npm install
