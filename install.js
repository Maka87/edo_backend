'use strict';

const fs = require('fs');
const path = require('path');

const TEMP_DIR = path.join(__dirname, 'temp');
const CACHE_DIR = path.join(__dirname, '.cache');

if (!fs.existsSync(TEMP_DIR)) {
    fs.mkdirSync(TEMP_DIR);
}

if (!fs.existsSync(CACHE_DIR)) {
  fs.mkdirSync(CACHE_DIR);
}
