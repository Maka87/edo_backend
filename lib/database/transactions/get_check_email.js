'use strict'

const { QueryTypes } = require('sequelize');
const config = require('config').util.toObject();
const request = require('request-promise-native');

/**
 * @typedef {import('../database.js')} Database
 */

/**
 * Returns services to EDO
 * @param {object} options
 * @param {String} options.email
 */
async function get_check_email(options) {
  let query = `${options.email}`;
  let send = await request({
    method: 'POST',
    headers: {
      "Authorization": "Token " + config.dadata.api_key,
      "X-Secret": config.dadata.secret_key,
      "Content-Type": "application/json",
    },
    uri: `https://cleaner.dadata.ru/api/v1/clean/email`,
    body: JSON.stringify([query])
  });
  return JSON.parse(send);
}

module.exports = get_check_email;