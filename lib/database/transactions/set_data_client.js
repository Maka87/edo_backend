'use strict'

const { QueryTypes } = require('sequelize');
const config = require('config').util.toObject();
const cache  = require('../../cache.js');
const WorkerAmqp = require('../../middleware/rabbit.js');
const { formatDate, formatDateLocal } = require('../../utils/date.js');
const CACHE_VALUES = require('../../../const/cache.js');

/**
 * @typedef {import('../database.js')} Database
 */

/**
 * Returns available tariffs
 * @param {object} options
 * @param {object} options.client
 * @param {number} options.docs
 * @param {String} options.firstname
 * @param {String} options.lastname
 * @param {String} options.middlename
 * @param {String} options.date
 * @param {String} options.phone
 * @param {String} options.email
 * @param {number} options.serial
 * @param {number} options.number
 * @param {string} options.issued
 * @param {Boolean} options.address
 * @param {Boolean} options.personal
 * @param {Boolean} options.oferta
 * @param {Boolean} options.technician
 * @param {Boolean} options.mailing
 * @param {Boolean} options.equipment
 * @param {String} options.registration
 * @param {String} options.ip
 * @param {String} options.departament_code
 */
function set_data_client(options) {
    /**
     * @type {Database}
     */
    const database = this;
    return database.transaction(async (transaction) => {
      const {  ClientValue, LogUserlog } = database.models;
      try {
        let phone = parseInt(options.phone.replace(/[^\d]/g, ''));
        let pass_sms = await Math.floor(Math.random() * (9999 - 1000 + 1)) + 1000;
        let sms_key = await ClientValue.findOne({
          where: {
            client_id: options.client.id,
            field_id: 34
          }
        });
        if(sms_key && sms_key.dataValues.length != 0) {
          if (formatDateLocal(sms_key.dataValues.changed, 5) == formatDateLocal(new Date())) {
            pass_sms = sms_key.dataValues.value;
          } else {
            await sms_key.update({
              value: pass_sms,
              changed: formatDate(new Date(), 5)
            });
          }
        } else {
          await ClientValue.create({
            client_id: options.client.id,
            field_id: 34,
            value: pass_sms,
            changed_emp_id: 0
          });
        }
        await LogUserlog.create({
          errno: 0,
          data: `Отправлено СМС с кодом подтверждения ${pass_sms} на номер ${phone}`,
          page: 'https://edo.profintel.ru',
          client_id: options.client.id,
          ip: options.ip,
          type_id: 1,
          item_id: options.client.id
        });
        let rabbit = new WorkerAmqp({key: pass_sms, phone});
        options.phone = phone;
        options.send_sms = pass_sms;
        await rabbit.start();
        let key = `client_${options.client.id}`;
        await cache.setCache(key, options, CACHE_VALUES.EXPIRES, CACHE_VALUES.UPDATES);
        return options;
      }
      catch (err) {
        console.log('ROLLBACK set_data_client');
        console.log(err);
        await transaction.rollback();
      }
      
    });
}

module.exports = set_data_client;
