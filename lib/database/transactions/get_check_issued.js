'use strict'

const { QueryTypes } = require('sequelize');
const config = require('config').util.toObject();
const request = require('request-promise-native');

/**
 * @typedef {import('../database.js')} Database
 */

/**
 * Returns services to EDO
 * @param {object} options
 * @param {string} options.issued
 */
async function get_check_issued(options) {
  const query = await request({
    method: 'POST',
    headers: {
      "Authorization": "Token " + config.dadata.api_key,
      "Content-Type": "application/json",
      "Accept": "application/json"
    },
    uri: `https://suggestions.dadata.ru/suggestions/api/4_1/rs/suggest/fms_unit`,
    body: {
      query: options.issued
    },
    json: true
  });
  return query;
}

module.exports = get_check_issued;