'use strict'

const { QueryTypes } = require('sequelize');
const config = require('config').util.toObject();
const cache  = require('../../cache.js');
const WorkerAmqp = require('../../middleware/rabbit.js');
const { formatDate, formatDateLocal } = require('../../utils/date.js');
const DOCUMENT = require('../../../const/document.js');
const SERVICES = require('../../../const/services.js');
const oferta = require('../../utils/gw/oferta.js');
const docsMain = require('../models/docs-main.js');

/**
 * @typedef {import('../database.js')} Database
 */

/**
 * Returns available tariffs
 * @param {object} options
 * @param {object} options.client
 * @param {number} options.docs
 * @param {String} options.firstname
 * @param {String} options.lastname
 * @param {String} options.middlename
 * @param {String} options.date
 * @param {String} options.phone
 * @param {String} options.email
 * @param {number} options.serial
 * @param {number} options.number
 * @param {string} options.issued
 * @param {Boolean} options.address
 * @param {Boolean} options.personal
 * @param {Boolean} options.oferta
 * @param {Boolean} options.technician
 * @param {Boolean} options.mailing
 * @param {Boolean} options.equipment
 * @param {String} options.registration
 * @param {String} options.ip,
 * @param {Number} options.send_sms,
 * @param {String} options.departament_code,
 * @param {object} options.services,
 */
function set_edo(options) {
    /**
     * @type {Database}
     */
    const database = this;
    return database.transaction(async (transaction) => {
      const { LogUserlog, Client, ClientValue, InetRekv, ClientPhone, DocsMain, ClientsPassport, ServicesClientsValues, DocsFilesHtmlClient, EdoServices, AddressSearchInfo } = database.models;
      try {
        if (options.client.t_kl === true) {
          let passport = await ClientsPassport.findOne({
            where: {
              client_id: options.client.id
            }
          });
          const [day, month, year] = options.date.split('.');
          
          const passport_data = {
            client_id: options.client.id,
            series: options.serial,
            number: options.number,
            date_issue: new Date(`${year}-${month}-${day}`),
            issued: options.issued,
            departament_code: options.departament_code
          };
          if (passport && passport.dataValues.length != 0) {
            await passport.update(passport_data);
          } else {
            await ClientsPassport.create(passport_data);
          }
        }
        options.services.forEach( async (service) => {
          let edo_active = await ServicesClientsValues.findOne({
            where: {
              cl_service_id: service.id,
              field_id: 71
            }
          })
          let params = {
            cl_service_id: service.id,
            field_id: 71,
            value: true
          }
          if (edo_active && edo_active.dataValues.length != 0) {
            await edo_active.update(params);
          } else {
            await ServicesClientsValues.create(params);
          }
          let doc = await DocsMain.findOne({
            where: {
              id: service.doc_id
            }
          });
          await doc.update({folder_id: DOCUMENT.DOC.FOLDER.EDO});
          
          if (service.service_id == SERVICES.SERVICE.INTERNET) {
            let inet_rekv = await InetRekv.findOne({
              where: {
                client_id: options.client.id,
                block: SERVICES.BLOCK_IP.CLIENT_OFERTA
              }
            });
            if (inet_rekv) {
              await inet_rekv.update({ block: SERVICES.BLOCK_IP.ACCESS_FULL })
            }
          }
          let {html} = await oferta.getHtmlDocoment({ client_id: options.client.id, doc_id: service.doc_id, service_id: service.service_id });
    
          let html_clients = await DocsFilesHtmlClient.findOne({
            where: {
              doc_id: service.doc_id
            }
          })
          if (html_clients && html_clients.dataValues.length != 0) {
            await html_clients.update({html})
          } else {
            let edo_params = await EdoServices.findOne({
              where: {
                service_id: service.service_id,
                t_kl: options.client.t_kl
              }
            });
            await DocsFilesHtmlClient.create({
              doc_id: service.doc_id,
              patterns_id: edo_params.dataValues.template_id,
              html
            })
          }
        });
        let paramsClient = {};
        if (options.client.t_kl === true) {
          options.issued
          let document = `${DOCUMENT.DOCUMENTS[options.docs]} серия: ${options.serial} номер: ${options.number} дата выдачи: ${options.date} кем выдан: ${options.issued}`;
          paramsClient.lastname = options.lastname;
          paramsClient.firstname = options.firstname;
          paramsClient.middlename = options.middlename;
          paramsClient.document = document;

          let client_value = await ClientValue.findOne({
            where: {
              client_id: options.client.id,
              field_id: 69
            }
          });
          let address_reg;
          if (options.registration == '') {
            let address = await AddressSearchInfo.findOne({
              where: {
                id: options.client.address_id
              }
            });
            address_reg = address.dataValues.addr_full;
          } else {
            address_reg = options.registration;
          }
          if (client_value && client_value.dataValues.length != 0) {
            await client_value.update({
              value: address_reg
            })
          } else {
            await ClientValue.create({
              client_id: options.client.id,
              field_id: 69,
              value: address_reg,
              changed_emp_id: 0
            })
          }
        } else {
          paramsClient.lastname = options.lastname;
          paramsClient.inn = options.inn;
          paramsClient.ur = options.address_ur;
        }
        if (paramsClient.length != 0) {
          let client = await Client.findOne({
            where: {
              id: options.client.id
            }
          })

          await client.update(paramsClient)
        }
        await LogUserlog.create({
          errno: 0,
          data: `Подтверждения кодом из СМС с номера ${options.phone}`,
          page: 'https://edo.profintel.ru',
          client_id: options.client.id,
          ip: options.ip,
          type_id: 1,
          item_id: options.client.id
        });
      }
      catch (err) {
          console.log('ROLLBACK set_edo');
          console.log(err);
          await transaction.rollback();
      }
      
    });
}

module.exports = set_edo;
