'use strict'

const { QueryTypes } = require('sequelize');
const config = require('config').util.toObject();
const request = require('request-promise-native');

/**
 * @typedef {import('../database.js')} Database
 */

/**
 * Returns services to EDO
 * @param {object} options
 * @param {Number} options.serial
 * @param {Number} options.number
 */
async function get_check_passport(options) {
  let query = `${options.serial} ${options.number}`;
  let send = await request({
    method: 'POST',
    headers: {
      "Authorization": "Token " + config.dadata.api_key,
      "X-Secret": config.dadata.secret_key,
      "Content-Type": "application/json",
    },
    uri: `https://cleaner.dadata.ru/api/v1/clean/passport`,
    body: JSON.stringify([query])
  });
  return JSON.parse(send);
}

module.exports = get_check_passport;