'use strict'

const { QueryTypes } = require('sequelize');
const config = require('config').util.toObject();
const getServicesRunQuery = require('../queries/get_services_run.js');
const getClientDevicesQuery = require('../queries/get_client_devices.js');

const { formatDateLocal } = require('../../../lib/utils/date.js');
const WORKS = require('../../../const/works.js');
const SERVICES = require('../../../const/services.js');
const ERROR = require('../../../const/error.js');

/**
 * @typedef {import('../database.js')} Database
 */

/**
 * Returns services to EDO
 * @param {object} options
 * @param {number} options.client_id
 */
function get_services_edo(options) {
  let database = this;
  let retval = [];
  return database.transaction(async (transaction) => {
    try {
      let { client_id } = options;
      let service_info = getServicesRunQuery({client_id});

      let edoInfo = await database.query(service_info.text, {
        replacements: service_info.replacements,
        plain: false,
        raw: true,
        type: QueryTypes.SELECT
      });
      
      let services = {
        status_work: true,
        data_work: [],
        time_work: [],
        services: [],
        devices: []
      };
      let error;
      let service_edo = false;
      if (edoInfo.length == 0) {
        error = ERROR.PAGES.NOT_SERVICES;
      } else {
        await edoInfo.forEach(async function (element) {
          if(element.edo_status == true) {
            if (element.work_status == WORKS.STATUS.EXPECTATION) {
              services.status_work = false;
              if (!element.work_date || !element.work_time) {
                error = ERROR.PAGES.NOT_SERVICES;
              } else {
                let str = ERROR.PAGES.NOT_SERVICES_WORK.MESSAGE.replace(/#DATA#/, formatDateLocal(element.work_date));
                str = str.replace(/#TIMES#/, element.work_time);
                error = {
                  MESSAGE: str,
                  TOOLTIP: '',
                };
              }
              return;
            }
            if (element.doc_folder != null) {
              error = ERROR.PAGES.NOT_DOCS;
              return;
            }
            if (!element.ip) {
              error = ERROR.PAGES.NOT_IP;
              return;
            }
            if (element.service_id == SERVICES.SERVICE.INTERNET && element.block != SERVICES.BLOCK_IP.CLIENT_OFERTA) {
              error = ERROR.PAGES.BLOCK_IP;
              return;
            }
            service_edo = true;
            services.data_work.push(element.work_date);
            services.time_work.push(element.work_time);
            services.services.push({
              id: element.cl_service_id,
              service_id: element.service_id,
              name: element.service_name,
              doc_id: element.doc_id,
              doc_folder: element.doc_folder,
              emp_name: element.emp_name,
              emp_phone: element.emp_phone,
              ip: element.ip,
              tarif_name: element.tarif_name,
            })
            let device_info = getClientDevicesQuery({work_id: element.work_id});
          
            let device = await database.query(device_info.text, {
              replacements: device_info.replacements,
              plain: false,
              raw: true,
              type: QueryTypes.SELECT
            });
            services.devices.push(device);
          }
        });
      }
      if (!service_edo && !error) {
        error = ERROR.PAGES.NOT_SERVICES_EDO;
      }
      if (error) {
        retval = {error};
      } else {
        retval = services;
      }
    } catch (err) {
      console.log('ROLLBACK get_services_run');
      console.log(err);
      await transaction.rollback();
    }
    return retval;
  });
}

module.exports = get_services_edo;