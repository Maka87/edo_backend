'use strict'

const config = require('config').util.toObject();
const { Sequelize } = require('sequelize');

const models = {
  clientModel: require('./models/client.js'),
  clientValueModel: require('./models/client-value.js'),
  clientPhoneModel: require('./models/client-phone.js'),
  inetRekvModel: require('./models/inet-rekv.js'),
  logUserlogModel: require('./models/log-userlog.js'),
  docsMainModel: require('./models/docs-main.js'),
  clientsPassportModel: require('./models/clients-passport.js'),
  servicesClientsValuesModel: require('./models/services-client-values.js'),
  docsFilesHtmlClientModel: require('./models/docs-files-html-client.js'),
  edoServicesModel: require('./models/edo-services.js'),
  addressSearchInfoModel: require('./models/address-search-info.js'),
};

const transactions = {
  get_services_run: require('./transactions/get_services_run.js'),
  get_check_issued: require('./transactions/get_check_issued.js'),
  get_check_email: require('./transactions/get_check_email.js'),
  get_check_passport: require('./transactions/get_check_passport.js'),
  set_data_client: require('./transactions/set_data_client.js'),
  set_edo: require('./transactions/set_edo.js'),
};

class Database extends Sequelize {
    /**
     * Creates an instance of Database
     */
    constructor() {
        super(config.database.name, config.database.username, config.database.password, config.database.options);

        this.models = {
          Client: models.clientModel.Model,
          ClientValue: models.clientValueModel.Model,
          ClientPhone: models.clientPhoneModel.Model,
          InetRekv: models.inetRekvModel.Model,
          LogUserlog: models.logUserlogModel.Model,
          DocsMain: models.docsMainModel.Model,
          ClientsPassport: models.clientsPassportModel.Model,
          ServicesClientsValues: models.servicesClientsValuesModel.Model,
          DocsFilesHtmlClient: models.docsFilesHtmlClientModel.Model,
          EdoServices: models.edoServicesModel.Model,
          AddressSearchInfo: models.addressSearchInfoModel.Model,
        };

        for (let key in models) {
            let { modelOptions, modelProperties, Model } = models[key];
            modelOptions.sequelize = this;
            Model.init(modelProperties, modelOptions);
        }

        this.transactions = transactions;

        for (let key in this.transactions) {
            this.transactions[key] = this.transactions[key].bind(this);
        }
    }
}

module.exports = Database;
