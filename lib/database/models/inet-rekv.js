'use strict'

const config = require('config').util.toObject();
const { Model, DataTypes } = require('sequelize');

class InetRekv extends Model { }

const modelOptions = {
    schema: 'public',
    tableName: 'inet_rekv'
};

const modelProperties = {
    ip: {
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    client_id: {
      type: DataTypes.INTEGER
    },
    block: {
      type: DataTypes.INTEGER
    },
    doc_id: {
      type: DataTypes.INTEGER
    },
    project_id: {
      type: DataTypes.INTEGER
    }
};

module.exports = {
    modelOptions,
    modelProperties,
    Model: InetRekv
};
