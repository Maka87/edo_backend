'use strict'

const config = require('config').util.toObject();
const { Model, DataTypes } = require('sequelize');

class DocsMain extends Model {}

const modelOptions = {
    schema: 'docs',
    tableName: 'main'
};

const modelProperties = {
    id: {
        primaryKey: true,
        autoIncrement: true,
        type: DataTypes.INTEGER
    },
    folder_id: {
        type: DataTypes.INTEGER
    }
};

module.exports = {
    modelOptions,
    modelProperties,
    Model: DocsMain
};
