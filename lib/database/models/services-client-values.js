'use strict'

const config = require('config').util.toObject();
const { Model, DataTypes } = require('sequelize');

class ServicesClientsValues extends Model {}

const modelOptions = {
    schema: 'services',
    tableName: 'clients_values'
};

const modelProperties = {
    id: {
        primaryKey: true,
        autoIncrement: true,
        type: DataTypes.INTEGER
    },
    cl_service_id: {
        type: DataTypes.INTEGER
    },
    field_id: {
        type: DataTypes.INTEGER
    },
    value: {
        type: DataTypes.TEXT
    }
};

module.exports = {
    modelOptions,
    modelProperties,
    Model: ServicesClientsValues
};
