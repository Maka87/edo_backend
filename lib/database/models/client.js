'use strict'

const config = require('config').util.toObject();
const { Model, DataTypes } = require('sequelize');

class Client extends Model { }

const modelOptions = {
    schema: 'public',
    tableName: 'clients'
};

const modelProperties = {
    id: {
        primaryKey: true,
        autoIncrement: true,
        type: DataTypes.INTEGER
    },
    firstname: {
        type: DataTypes.STRING(50)
    },
    middlename: {
        type: DataTypes.STRING(50)
    },
    lastname: {
        type: DataTypes.STRING(250)
    },
    email: {
        type: DataTypes.STRING(300)
    },
    address_id: {
        type: DataTypes.INTEGER
    },
    station_id: {
        type: DataTypes.INTEGER
    },
    ksp_id: {
        type: DataTypes.INTEGER
    },
    t_kl: {
        type: DataTypes.BOOLEAN
    },
    document: {
        type: DataTypes.STRING(250)
    },
    inn: {
        type: DataTypes.STRING(60)
    },
    ua: {
        type: DataTypes.STRING(300)
    }
};

module.exports = {
    modelOptions,
    modelProperties,
    Model: Client
};
