'use strict'

const config = require('config').util.toObject();
const { Model, DataTypes } = require('sequelize');

class ClientPhone extends Model {
    static TYPE = {
        MOBILE: 2
    }

    /**
     * Returns client mobile phone
     * @param {number} client_id
     */
    static async get(client_id) {
        const item = await this.findOne({
            where: {
                client_id,
                type_id: this.TYPE.MOBILE
            }
        });

        if (item) {
            return item.dataValues.number;
        }

        return undefined;
    }
}

const modelOptions = {
    schema: 'public',
    tableName: 'client_phones'
};

const modelProperties = {
    id: {
        primaryKey: true,
        autoIncrement: true,
        type: DataTypes.INTEGER
    },
    client_id: {
        type: DataTypes.INTEGER
    },
    type_id: {
        type: DataTypes.INTEGER
    },
    number: {
        type: DataTypes.BIGINT
    }
};

module.exports = {
    modelOptions,
    modelProperties,
    Model: ClientPhone
};
