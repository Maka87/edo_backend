'use strict'

const uuid = require('uuid');

const config = require('config').util.toObject();
const { Model, DataTypes } = require('sequelize');

class ClientValue extends Model {
    static DEFAULT = {
        CHANGED_EMP_ID: 0
    }

    static FIELDS = {
        AUTOPAYMENT_SUM: 31,
        UUID: 36
    }

    /**
     * Returns client value
     * @param {string} value
     * @param {number} field_id
     */
    static async get(value, field_id) {
        const item = await this.findOne({
            where: {
                value,
                field_id
            }
        });

        if (item) {
            return item.dataValues.client_id;
        }

        return undefined;
    }
}

const modelOptions = {
    schema: 'clients',
    tableName: 'values'
};

const modelProperties = {
    id: {
        primaryKey: true,
        autoIncrement: true,
        type: DataTypes.INTEGER
    },
    client_id: {
        type: DataTypes.INTEGER
    },
    field_id: {
        type: DataTypes.INTEGER
    },
    value: {
        type: DataTypes.TEXT
    },
    changed: {
        type: DataTypes.DATE
    },
    changed_emp_id: {
        type: DataTypes.INTEGER
    }
};

module.exports = {
    modelOptions,
    modelProperties,
    Model: ClientValue
};
