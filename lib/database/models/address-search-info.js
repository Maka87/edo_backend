'use strict'

const config = require('config').util.toObject();
const { Model, DataTypes } = require('sequelize');

class AddressSearchInfo extends Model { }

const modelOptions = {
    schema: 'address',
    tableName: 'search_info'
};

const modelProperties = {
    id: {
      primaryKey: true,
      autoIncrement: false,
      type: DataTypes.INTEGER
    },
    addr_full: {
      type: DataTypes.STRING(1000)
    }
};

module.exports = {
    modelOptions,
    modelProperties,
    Model: AddressSearchInfo
};
