'use strict'

const config = require('config').util.toObject();
const { Model, DataTypes } = require('sequelize');

class ClientsPassport extends Model {}

const modelOptions = {
    schema: 'clients',
    tableName: 'passport_data'
};

const modelProperties = {
    id: {
        primaryKey: true,
        autoIncrement: true,
        type: DataTypes.INTEGER
    },
    client_id: {
        type: DataTypes.INTEGER
    },
    series: {
        type: DataTypes.STRING(4)
    },
    number: {
        type: DataTypes.STRING(6)
    },
    date_issue: {
        type: DataTypes.DATE
    },
    issued: {
        type: DataTypes.TEXT
    },
    departament_code: {
        type: DataTypes.STRING(7)
    }
};

module.exports = {
    modelOptions,
    modelProperties,
    Model: ClientsPassport
};
