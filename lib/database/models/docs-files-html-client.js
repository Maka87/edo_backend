'use strict'

const config = require('config').util.toObject();
const { Model, DataTypes } = require('sequelize');

class DocsFilesHtmlClient extends Model {}

const modelOptions = {
    schema: 'docs',
    tableName: 'files_html_clients'
};

const modelProperties = {
    id: {
        primaryKey: true,
        autoIncrement: true,
        type: DataTypes.INTEGER
    },
    doc_id: {
        type: DataTypes.INTEGER
    },
    patterns_id: {
        type: DataTypes.INTEGER
    },
    html: {
        type: DataTypes.TEXT
    }
};

module.exports = {
    modelOptions,
    modelProperties,
    Model: DocsFilesHtmlClient
};
