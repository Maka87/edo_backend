'use strict'

const config = require('config').util.toObject();
const { Model, DataTypes } = require('sequelize');

class LogUserlog extends Model { }

const modelOptions = {
    schema: 'log',
    tableName: 'userlog'
};

const modelProperties = {
    id: {
        primaryKey: true,
        autoIncrement: true,
        type: DataTypes.INTEGER
    },
    errno: {
        type: DataTypes.INTEGER
    },
    data: {
        type: DataTypes.STRING(250)
    },
    page: {
        type: DataTypes.STRING(300)
    },
    client_id: {
        type: DataTypes.INTEGER
    },
    ip: {
        type: DataTypes.STRING(100)
    },
    type_id: {
      type: DataTypes.INTEGER
    },
    item_id: {
      type: DataTypes.INTEGER
    }
};

module.exports = {
    modelOptions,
    modelProperties,
    Model: LogUserlog
};
