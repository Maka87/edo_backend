'use strict'

const config = require('config').util.toObject();
const { Model, DataTypes } = require('sequelize');

class EdoServices extends Model {}

const modelOptions = {
    schema: 'public',
    tableName: 'edo_services'
};

const modelProperties = {
    id: {
        primaryKey: true,
        autoIncrement: true,
        type: DataTypes.INTEGER
    },
    service_id: {
        type: DataTypes.INTEGER
    },
    ksp_ids: {
        type: DataTypes.ARRAY(DataTypes.INTEGER)
    },
    t_kl: {
        type: DataTypes.BIGINT
    },
    template_id: {
        type: DataTypes.INTEGER
    }
};

module.exports = {
    modelOptions,
    modelProperties,
    Model: EdoServices
};
