'use strict'

const env = process.env.NODE_ENV;

const Database = require('./database.js');

const database = new Database();

module.exports = {
    database,
    syncPromise: env === 'production' ? null : database.sync({ force: false })
};
