'use strict'

const WORKS = require('../../../const/works.js');

/**
 * @param {object} options
 * @param {number} options.work_id
 */

function getClientDevicesQuery(options) {
  const { work_id } = options;
  const text = `SELECT
      di.name as name,
      sm.name as serial_num
    from projects.items_act     pia
      left join devcatalog.items di ON di.id = pia.item_id
      LEFT JOIN sklad.main       sm ON sm.id = pia.sklad_main_id
    where pia.work_id = :work_id
    and pia.sklad_main_id is not null`;

  const replacements = {
    work_id
  };

  return {
    text,
    replacements
  }
}

module.exports = getClientDevicesQuery;