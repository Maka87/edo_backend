'use strict'

const WORKS = require('../../../const/works.js');
const SERVICES = require('../../../const/services.js');

/**
 * @param {object} options
 * @param {number} options.client_id
 */

function getServicesRunQuery(options) {
  const { client_id } = options;
  
  const text = `SELECT DISTINCT
      pw.id                                                           as work_id,
      pw.data                                                         as work_date,
      pw.times                                                        as work_time,
      pw.status_id                                                    as work_status,
      cs.id                                                           as cl_service_id,
      cs.service_id                                                   as service_id,
      cs.doc_id                                                       as doc_id,
      dm.folder_id                                                    as doc_folder,
      e.name                                                          as emp_name,
      e.phone                                                         as emp_phone,
      CASE WHEN cs.service_id = 1 THEN ir.ip ELSE null END            as ip,
      CASE WHEN cs.service_id = 1 THEN ir.block ELSE null END         as block,
      CASE WHEN es.id is not null THEN true ELSE false END            as edo_status,
      CASE WHEN cs.service_id = 1 THEN it.name ELSE st.tarif_name END as tarif_name,
      s.name                                                          as service_name
    FROM projects.works               pw
      INNER JOIN projects.subprojects ps ON ps.id            = pw.subproject_id
      INNER JOIN projects.pr_main     pm ON pm.id            = ps.project_id
      INNER JOIN clients               c ON c.id             = pm.client_id
      INNER JOIN cl_services          cs ON cs.project_id    = pm.id
      LEFT  JOIN docs.main            dm ON dm.id            = cs.doc_id
      LEFT  JOIN services              s ON s.id             = cs.service_id
      LEFT  JOIN inet_clients_tarif  ict ON ict.client_id    = c.id
      LEFT  JOIN inet_tarif           it ON it.id            = ict.tarif_id
      LEFT  JOIN tv.users             tu ON tu.cl_service_id = cs.id
      LEFT  JOIN tv.user_tarif       tut ON tut.user_id      = tu.id
      LEFT  JOIN services.tarifs      st ON st.tarif_id      = tut.tarif_id OR st.tarif_id = cs.tarif_id
      LEFT  JOIN projects.workers    pws ON pws.work_id      = pw.id
      LEFT  JOIN emp                   e ON e.id             = pws.emp_id
      LEFT  JOIN inet_rekv            ir ON ir.client_id     = c.id OR ir.project_id = pm.id
      LEFT  JOIN edo_services         es ON es.service_id    = cs.service_id AND c.ksp_id = ANY (es.ksp_ids)
    WHERE true --pw.data <= NOW()
      AND pw.status_id <= ${WORKS.STATUS.EXECUTION}
      AND c.id = :client_id`;

  const replacements = {
    client_id
  };

  return {
    text,
    replacements
  }
}

module.exports = getServicesRunQuery;