'use strict'

const STATUS = require('../../const/response-status.js');

class HTTPError extends Error {
    /**
     * Creates an instance of HTTPError
     * @param {number} code
     * @param {string} message
     */
    constructor(code, message) {
        super(message);
        this.httpError = true;
        this.code = code;
    }
}

class NoContentError extends HTTPError {
    /**
     * Creates an instance of NoContentError
     * @param {string} [message]
     */
    constructor(message) {
        super(STATUS.NO_CONTENT.CODE, message || STATUS.NO_CONTENT.MESSAGE);
    }
}

class BadRequestError extends HTTPError {
    /**
     * Creates an instance of BadRequestError
     * @param {string} [message]
     */
    constructor(message) {
        super(STATUS.BAD_REQUEST.CODE, message || STATUS.BAD_REQUEST.MESSAGE);
    }
}

class UnauthorizedError extends HTTPError {
    /**
     * Creates an instance of UnauthorizedError
     * @param {string} [message]
     */
    constructor(message) {
        super(STATUS.UNAUTHORIZED.CODE, message || STATUS.UNAUTHORIZED.MESSAGE);
    }
}

class ForbiddenError extends HTTPError {
    /**
     * Creates an instance of ForbiddenError
     * @param {string} [message]
     */
    constructor(message) {
        super(STATUS.FORBIDDEN.CODE, message || STATUS.FORBIDDEN.MESSAGE);
    }
}

class NotFoundError extends HTTPError {
    /**
     * Creates an instance of NotFoundError
     * @param {string} [message]
     */
    constructor(message) {
        super(STATUS.NOT_FOUND.CODE, message || STATUS.NOT_FOUND.MESSAGE);
    }
}

class InternalServerError extends HTTPError {
    /**
     * Creates an instance of InternalServerError
     * @param {string} [message]
     */
    constructor(message) {
        super(STATUS.INTERNAL_SERVER_ERROR.CODE, message || STATUS.INTERNAL_SERVER_ERROR.MESSAGE);
    }
}

module.exports = {
    HTTPError,
    NoContentError,
    BadRequestError,
    ForbiddenError,
    NotFoundError,
    UnauthorizedError,
    InternalServerError
};
