'use strict'

const crypto = require('crypto');
const fs = require('fs').promises;
const path = require('path');
const child = require('child_process');
const logger = require('../logger.js');
const { unlinkIfExists } = require('./fs.js');

const TEMP_DIR = path.join(__dirname, '..', '..', 'temp');

const TIMEOUT = 20000;

/**
 * Creates PDF document from html and returns its bytes
 * @param {string} html
 * @returns {Promise<Buffer>}
 */
function htmlToPdf(html) {
    return new Promise(async (resolve, reject) => {
        const now = Date.now();
        const random = Math.random();

        const srcTempFileName = `${crypto.createHash('sha1').update(`${now}${random}`).digest('hex')}.html`;
        const dstTempFileName = `${crypto.createHash('sha1').update(`${now}${random}`).digest('hex')}.pdf`;
        
        const srcTempFilePath = path.join(TEMP_DIR, srcTempFileName);
        const dstTempFilePath = path.join(TEMP_DIR, dstTempFileName);

        await fs.writeFile(srcTempFilePath, html, 'utf8');

        let wkhtmltopdf;

        const errorHandler = async (err) => {
            if (wkhtmltopdf) {
                wkhtmltopdf.removeAllListeners('exit');
            }

            reject(err);

            await unlinkIfExists(srcTempFilePath);
            await unlinkIfExists(dstTempFilePath);
        };

        const dataHandler = async (data) => {
            if (wkhtmltopdf) {
                wkhtmltopdf.removeAllListeners('exit');
            }

            resolve(data);

            await unlinkIfExists(srcTempFilePath);
            await unlinkIfExists(dstTempFilePath);
        };

        try {
            wkhtmltopdf = child.spawn('wkhtmltopdf', [srcTempFilePath, dstTempFilePath]);

            wkhtmltopdf.on('error', console.error);

            wkhtmltopdf.once('exit', async (code) => {
                wkhtmltopdf.removeAllListeners('error');
                if (code) {
                    await errorHandler(new Error(`wkhtmltopdf exited with code ${code}`));
                }
                else {
                    try {
                        const data = await fs.readFile(dstTempFilePath);
                        await dataHandler(data);
                    }
                    catch (err) {
                        await errorHandler(err);
                    }
                }
            });
        }
        catch (err) {
            await errorHandler(err);
        }

        setTimeout(async () => {
            await errorHandler(new Error(`Timeout`));
        }, TIMEOUT);
    });
}

module.exports = {
    htmlToPdf
};
