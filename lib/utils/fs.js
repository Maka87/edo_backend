'use strict'

const fs = require('fs').promises;

/**
 * Returns stats if file exists and null if not
 * @param {string} filePath
 */
async function safeStat(filePath) {
    try {
        return await fs.stat(filePath);
    }
    catch (err) {
        if (err.code == 'ENOENT') {
            return null;
        }
        throw new Error(err);
    }
}

/**
 * Unlinks file and returns true if it exists and false otherwise
 * @param {string} filePath 
 */
async function unlinkIfExists(filePath) {
    const stats = await safeStat(filePath);
    if (stats) {
        await fs.unlink(filePath);
        return true;
    }
    return false;
}

module.exports = {
    safeStat,
    unlinkIfExists
};
