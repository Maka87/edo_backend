'use strict'

const request = require('request-promise-native');
const config = require('config').util.toObject();

/**
 * Find address id
 * @param {object} options
 * @param {String} options.search
 */
async function getHtmlDocoment(options) {
    const { address_id } = await request({
      method: 'POST',
      uri: config.gw.url + `dadata_proxy/appserv.php`,
      body: {
        address: options.search
      },
      json: true
    });
    return address_id;
}

module.exports = {getHtmlDocoment};
