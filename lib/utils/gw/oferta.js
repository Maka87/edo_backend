'use strict'

const request = require('request-promise-native');
const config = require('config').util.toObject();

/**
 * Get html doc
 * @param {object} options
 * @param {number} options.client_id
 * @param {number} options.doc_id
 * @param {number} options.service_id
 */
async function getHtmlDocoment(options) {
    const { client_id, doc_id, service_id } = options;
    const response = await request({
        method: 'POST',
        uri: config.gw.url+ 'profintel/appserv.php',
        form: {
            command: 'getHtmlOferta',
            client_id,
            doc_id,
            service_id
        }
    });
    let res = JSON.parse(response)
    return res.result;
}

module.exports = {getHtmlDocoment};
