'use strict'

/**
 * Formats date to YYYY-MM-DD hh:mm:ss
 * @param {Date | string | number} date
 * @param {number} timezoneOffset
 */
function formatDate(date, timezoneOffset = 0) {
    const dateObj = new Date(date);
    dateObj.setUTCHours(dateObj.getUTCHours() + timezoneOffset);
    return dateObj.toISOString().replace('T', ' ').replace(/(\.\d+?)\Z?$/, '');
}

/**
 * Formats date to DD.MM.YYYY
 * @param {Date | string | number} date
 */
function formatDateLocal(date, timezoneOffset = 0) {
    const dateObj = new Date(date);

    dateObj.setUTCHours(dateObj.getUTCHours() + timezoneOffset);

    const day = dateObj.getUTCDate();
    const month = dateObj.getUTCMonth() + 1;
    const year = dateObj.getFullYear();

    return `${String(day).padStart(2, '0')}.${String(month).padStart(2, '0')}.${year}`;
}

module.exports = {
    formatDate,
    formatDateLocal
};