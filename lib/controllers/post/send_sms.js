'use strict'

const config = require('config').util.toObject();
const { database } = require('../../database/shared-instance.js');
const { HTTPError } = require('../../utils/http-error.js');
const STATUS = require('../../../const/response-status.js');
const ERROR = require('../../../const/error.js');
const { LogUserlog } = database.models;
const checkClient = require('../../middleware/checkClient.js');
const cache  = require('../../cache.js');
const WorkerAmqp = require('../../middleware/rabbit.js');
const CACHE_VALUES = require('../../../const/cache.js');

const prefix_phone = ['343', '800', '000'];

/**
 * @typedef {import("koa").Context} Context
 */

/**
 * Handles HTTP request
 * @param {Context} ctx
 */
async function handler(ctx) {
  const client = ctx.client;
  const { new_phone } = ctx.request.body;
  let data = await cache.getCache(`client_${client.id}`);

  try {
    if(new_phone) {
      if (prefix_phone.includes(new_phone.substr(4,3))) {
        throw ERROR.FORM.phone;
      }
      let phone = parseInt(new_phone.replace(/[^\d]/g, ''));
      if(data.phone != phone) {
        data.phone = phone;
        await cache.setCache(`client_${client.id}`, data, CACHE_VALUES.EXPIRES, CACHE_VALUES.UPDATES);
      }
    }
    await LogUserlog.create({
      errno: 0,
      data: `Отправлено СМС с кодом подтверждения ${data.send_sms} на номер ${data.phone}`,
      page: 'https://localhost',
      client_id: client.id,
      ip: data.ip,
      type_id: 1,
      item_id: client.id
    });
    let rabbit = new WorkerAmqp({key: data.send_sms, phone: data.phone});
    await rabbit.start();
    ctx.body = {result: 'OK'};
  } catch (err) {
    ctx.body = {error: err};
  } 
 }

 module.exports = {
  route: `${config.server.apiBaseRoute}/send_sms`,
  handler: [checkClient,  handler]
};