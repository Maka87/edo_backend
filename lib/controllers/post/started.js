'use strict'

const config = require('config').util.toObject();
const { database } = require('../../database/shared-instance.js');
const ERROR = require('../../../const/error.js');
const checkClient = require('../../middleware/checkClient.js');

const prefix_phone = ['343', '800', '000'];

/**
 * @typedef {import("koa").Context} Context
 */

/**
 * Handles HTTP request
 * @param {Context} ctx
 */
async function handler(ctx) {
  const client = ctx.client;
  const ip = ctx.request.header["x-forwarded-for"];
  const { docs,firstname,lastname,middlename,date,phone,email,serial,number,issued,address,personal,oferta,technician,mailing,equipment,registration, services, commitments, inn, address_ur } = ctx.request.body;
  try {
    if (client.t_kl === true) {
      if (!serial) throw ERROR.FORM.serial
      if(!number) throw ERROR.FORM.nomber
      const [day, month, year] = date.split('.');
    
      let data = new Date(year, month, day);
      
      if (data.getFullYear() != year && data.getMonth() != month && data.getDate() != day) {
        throw ERROR.FORM.date;
      }
    } else {
      if (!commitments) throw ERROR.FORM.technician
      if (!inn) throw ERROR.FORM.inn
      if (!address_ur) throw ERROR.FORM.address_ur
    }
    if (!personal) throw ERROR.FORM.personal
    if (!oferta) throw ERROR.FORM.oferta
    if (!technician) throw ERROR.FORM.technician
    
    if (prefix_phone.includes(phone.substr(4,3))) {
      throw ERROR.FORM.phone;
    }
    
    if (email) {
      let checkEmail = await database.transactions.get_check_email({email});
      if (checkEmail[0].qc == 1) throw ERROR.FORM.email
      if(client.t_kl == true && checkEmail[0].type != 'PERSONAL') throw ERROR.FORM.email_not_personal;
    }

    
    let departament_code;
    if (client.t_kl === true) {
      if (docs == 1) {
        let Issued = await database.transactions.get_check_issued({issued});
        if(!Issued.suggestions[0]) throw ERROR.FORM.issued;
        departament_code = Issued.suggestions[0].data.code;
        let passport = await database.transactions.get_check_passport({serial: serial, number: number});
        if (passport[0].qc == 1 || passport[0].qc == 10) {
          let err = passport[0].qc == 1 ? ERROR.PASSPORT[1] : ERROR.PASSPORT[10];
          throw err;
        }
      }
      if(!firstname) throw ERROR.FORM.firstname;
      if(!lastname) throw ERROR.FORM.lastname;
      if(!middlename && docs == 1) throw ERROR.FORM.middlename;
      if(address != true && registration == '') throw ERROR.FORM.registration;
    } else {
      if(!lastname) throw ERROR.FORM.name_ur;
    }
    let filledData = await database.transactions.set_data_client({
      client,
      docs,
      firstname,
      lastname,
      middlename,
      date,
      phone,
      email,
      serial,
      number,
      issued,
      address,
      personal,
      oferta,
      technician,
      mailing,
      equipment,
      registration,
      ip,
      departament_code,
      services,
      inn,
      address_ur,
      commitments})
    ctx.body = {result: 'OK', data: filledData};
  } catch (err) {
    ctx.body = {error: err};
  } 
 }

 module.exports = {
  route: `${config.server.apiBaseRoute}/started`,
  handler: [checkClient,  handler]
};