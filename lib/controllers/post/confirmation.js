'use strict'

const config = require('config').util.toObject();
const { database } = require('../../database/shared-instance.js');
const ERROR = require('../../../const/error.js');

const checkClient = require('../../middleware/checkClient.js');
const cache  = require('../../cache.js');
const CACHE_VALUES = require('../../../const/cache.js');

/**
 * @typedef {import("koa").Context} Context
 */

/**
 * Handles HTTP request
 * @param {Context} ctx
 */
async function handler(ctx) {
  const client = ctx.client;
  const { sms_key } = ctx.request.body;
  let data = await cache.getCache(`client_${client.id}`);

  try {
    if (data.send_sms != sms_key) throw ERROR.FORM.wrong_code;
    await database.transactions.set_edo(data);
    data.registration = true;
    await cache.setCache(`client_${client.id}`, data, CACHE_VALUES.EXPIRES, CACHE_VALUES.UPDATES);
    ctx.body = {result: 'OK'};
  } catch (err) {
    ctx.body = {error: err};
  } 
 }

 module.exports = {
  route: `${config.server.apiBaseRoute}/confirmation`,
  handler: [checkClient,  handler]
};