'use strict'

const config = require('config').util.toObject();
const { database } = require('../../database/shared-instance.js');
const { HTTPError } = require('../../utils/http-error.js');
const cache  = require('../../cache.js');

const checkClient = require('../../middleware/checkClient.js');

const STATUS = require('../../../const/response-status.js');

/**
 * @typedef {import("koa").Context} Context
 */

/**
 * Handles HTTP request
 * @param {Context} ctx
 */
async function handler(ctx) {
  try {
    const client_id = ctx.client.id;
    let clientCache =  await cache.cacheStatus(`client_${client_id}`);
    if (clientCache != 'valid') {
      let edo = await database.transactions.get_services_run({client_id});
      if (edo.error) {
        ctx.body = edo;
      } else {
        if (clientCache != 'none') {
          let data = await cache.getCache(`client_${client_id}`);
          let services = [];
          for(let service of edo.services) {
            for(let service_edo of data.services) {
              if (service.id != service_edo.id) {
                services.push(service);
              }
            }
          }
          if (services.length == 0) {
            ctx.body = {
              clientInfo: ctx.client,
              registartion: data.registration ? true : false,
              sms_send: true,
              data
            };
            return;
          }
          edo.services = services;
        }
        ctx.body = {
          clientInfo: ctx.client,
          services: edo
        };
      }
    } else {
      let data = await cache.getCache(`client_${client_id}`);
      ctx.body = {
        clientInfo: ctx.client,
        registartion: data.registration ? true : false,
        sms_send: true,
        data
      };
    }
  }
  catch (err) {
    if (err instanceof HTTPError) {
      ctx.body = {
          msg: err.message
      };
      ctx.status = err.code;
    }
    else {
      console.log(err);
      ctx.body = {
          msg: STATUS.INTERNAL_SERVER_ERROR.MESSAGE
      };
      ctx.status = STATUS.INTERNAL_SERVER_ERROR.CODE;
    }
  }
}

module.exports = {
  route: `${config.server.apiBaseRoute}/form`,
  handler: [checkClient, handler]
};
