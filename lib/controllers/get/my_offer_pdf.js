'use strict'

const config = require('config').util.toObject();
const { database } = require('../../database/shared-instance.js');
const { HTTPError } = require('../../utils/http-error.js');
const oferta = require('../../utils/gw/oferta.js');
const { htmlToPdf } = require('../../utils/pdf.js');
const ERROR = require('../../../const/error.js');
const cache  = require('../../cache.js');

const checkClient = require('../../middleware/checkClient.js');

const STATUS = require('../../../const/response-status.js');

/**
 * @typedef {import("koa").Context} Context
 */

/**
 * Handles HTTP request
 * @param {Context} ctx
 */
async function handler(ctx) {
    try {
        const client_id = ctx.client.id;
        let clientCache =  await cache.cacheStatus(`client_${client_id}`);
        let edo;
        if (clientCache != 'valid') {
            edo = await database.transactions.get_services_run({client_id});
        } else {
            edo = await cache.getCache(`client_${client_id}`);
        }
        let services = edo.services;
        let htmls = '';
        for(let service of services) {
            let {html} = await oferta.getHtmlDocoment({ client_id, doc_id: service.doc_id, service_id: service.service_id });
            htmls = htmls + html;
        }
        const pdf = await htmlToPdf(htmls);
        
        ctx.type = 'application/pdf';
        ctx.body = pdf;
    }
    catch (err) {
        if (err instanceof HTTPError) {
            ctx.body = {
                msg: err.message
            };
            ctx.status = err.code;
        }
        else {
            console.log(err);
            ctx.body = {
                msg: STATUS.INTERNAL_SERVER_ERROR.MESSAGE
            };
            ctx.status = STATUS.INTERNAL_SERVER_ERROR.CODE;
        }
    }
}

module.exports = {
    route: `${config.server.apiBaseRoute}/offer_pdf`,
    handler: [checkClient, handler]
};
