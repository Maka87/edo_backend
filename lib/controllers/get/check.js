'use strict'

const config = require('config').util.toObject();
const { database } = require('../../database/shared-instance.js');
const { HTTPError } = require('../../utils/http-error.js');
const STATUS = require('../../../const/response-status.js');

/**
 * @typedef {import("koa").Context} Context
 */

/**
 * Handles HTTP request
 * @param {Context} ctx
 */
 async function handler(ctx) {
  try {
    const { issued } = ctx.request.query;
    let edo = await database.transactions.get_check_issued({issued});
    ctx.body = {
      edo
    }
  }
  catch (err) {
    if (err instanceof HTTPError) {
      ctx.body = {
          msg: err.message
      };
      ctx.status = err.code;
    }
    else {
      console.log(err);
      ctx.body = {
          msg: STATUS.INTERNAL_SERVER_ERROR.MESSAGE
      };
      ctx.status = STATUS.INTERNAL_SERVER_ERROR.CODE;
    }
  }
}

module.exports = {
  route: `${config.server.apiBaseRoute}/check`,
  handler: [handler]
};
