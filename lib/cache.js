'use strict'

const fs = require('fs').promises;
const crypto = require('crypto');
const path = require('path');

const CACHE_KEY_MAX_LENGTH = 250;
const CACHE_DIR = path.join(__dirname, '..', '.cache');

/**
 * Returns stats if file exists and null if not
 * @param {string} filepath
 */
async function exists(filepath) {
  try {
    return await fs.stat(filepath);
  }
  catch (err) {
    if (err.code == 'ENOENT') {
      return null;
    }
    throw new Error(err);
  }
}

/**
 * Replaces key with its hash if it exceeds max length
 * @param {string} key 
 */
function trimCacheKey(key) {
  return key.length <= CACHE_KEY_MAX_LENGTH ? key : crypto.createHash('sha512').update(key).digest('hex');
}

/**
 * Returns cache status for the given key
 * @param {string} key 
 */
async function cacheStatus(key) {
  const filepath = path.join(CACHE_DIR, `${trimCacheKey(key)}.json`);
  if (await exists(filepath)) {
    try {
      const obj = JSON.parse(await fs.readFile(filepath, 'utf8'));
      if (obj.expiresAt) {
        if (obj.expiresAt > Date.now()) {
          if (obj.updatesAt && obj.updatesAt < Date.now()) {
            return 'updatable';
          }
          return 'valid';
        }
        return 'outdated';
      }
      return 'valid';
    }
    catch (err) {
      try {
        await fs.unlink(filepath);
      }
      catch (err) { }
      return 'invalid';
    }
  }
  return 'none';
}

/**
 * Gets cache for the given key
 * @param {string} key
 * @param {boolean} onlyFresh
 */
async function getCache(key, onlyFresh) {
  const filepath = path.join(CACHE_DIR, `${trimCacheKey(key)}.json`);
  if (await exists(filepath)) {
    try {
      const obj = JSON.parse(await fs.readFile(filepath, 'utf8'));
      return obj.value;
    }
    catch (err) {
      try {
        await fs.unlink(filepath);
      }
      catch (err) { }
      return null;
    }
  }
  return null;
}

/**
 * Sets cache for the given key
 * @param {string} key 
 * @param {any} value 
 * @param {number} [expiresAfter] A period of time in miliseconds during which cache is valid
 * @param {number} [updatesAfter] A period of time in miliseconds during which cache does not need to be updated
 */
async function setCache(key, value, expiresAfter, updatesAfter) {
  const filepath = path.join(CACHE_DIR, `${trimCacheKey(key)}.json`);
  console.log(filepath);
  let obj = { value };
  let now = Date.now();
  if (expiresAfter) {
    obj.expiresAt = now + expiresAfter;
    obj.updatesAt = now + updatesAfter;
  }
  await fs.writeFile(filepath, JSON.stringify(obj));
}

module.exports = {
  cacheStatus,
  getCache,
  setCache
};
