'use strict'

const fs = require('fs');
const path = require('path');
const Router = require('koa-router');
const router = new Router();

const CONTROLLER_DIR = path.join(__dirname, 'controllers');

const methods = ['get', 'post'];

for (const method of methods) {
    const METHOD_CONTROLLER_DIR = path.join(CONTROLLER_DIR, method);
    const controllerFiles = fs.readdirSync(METHOD_CONTROLLER_DIR);
    for (const controllerFile of controllerFiles) {
        const controller = require(path.join(METHOD_CONTROLLER_DIR, controllerFile));
        if (typeof controller.route == 'string') {
            if (Array.isArray(controller.handler)) {
                router[method](controller.route, ...controller.handler);
            }
            else if (typeof controller.handler == 'function') {
                router[method](controller.route, controller.handler);
            }
        }
    }
}

module.exports = {
    router
};
