'use strict'

const config = require('config').util.toObject();
const request = require('request-promise-native');
const { database } = require('../database/shared-instance.js');
const { Client, ClientValue, ClientPhone, InetRekv } = database.models;
const ERROR = require('../../const/error.js');
/**
 * @typedef {import("koa").Context} Context
 */

/**
 * Handles HTTP request
 * @param {Context} ctx
 * @param {function} next
 */
async function getClient(ctx, next) {
  const { hash } = ctx.request.query;
  const ip = ctx.request.header["x-forwarded-for"];
  let client_id;
  if (!hash || hash == 'undefined') {
    if(ip) {
      let clientIp = await InetRekv.findOne({
        where: {
          ip
        }
      });
      if (clientIp === null) {
        ctx.client = {}
        await next();
        return;  
      }
      client_id = clientIp.client_id;
    } else {
      ctx.client = {}
      await next();
      return;
    }
  }
  if(!client_id) {
    client_id = await ClientValue.get(hash, ClientValue.FIELDS.UUID);
  }
  if (!client_id) {
    ctx.client = {}
    await next();
    return;
  }
  let client    = await Client.findOne({
    where: {
      id: client_id
    }
  })
  let client_phone = await ClientPhone.get(client_id);
  if (!client) {
    ctx.client = {}
  } else {
    ctx.client = {
      id: client.id,
      firstname: client.firstname,
      middlename: client.middlename,
      lastname: client.lastname,
      email: client.email,
      address_id: client.address_id,
      station_id: client.station_id,
      ksp_id: client.ksp_id,
      phone: client_phone,
      t_kl: client.t_kl,
      inn: client.inn,
      ua: client.ua,
    };
  }
  await next();
}
module.exports = getClient;