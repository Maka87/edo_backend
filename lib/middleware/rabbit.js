'use strict'

process.env.IS_FORKED = 'true';

const config = require('config').util.toObject();
const amqplib = require('amqplib/callback_api');

class WorkerAmqp {

  
  /**
   * 
   * @param {object} options
   */
  constructor (options) {
    this.key = options.key;
    this.phone = options.phone;
    this.channel = null;
  }

  start () {
    amqplib.connect(config.rabbitmq.url, async (err, conn) => {
      if (err) this.close(err);
      try {
        await this.publisher(conn)
      } catch (err) {
        return err;
      }
      return;
    });
  }

  publisher (conn) {
    try {
      var channel = conn.createChannel();
      channel.on('error', (err) => {
        throw {err: `[AMQP] Error connected: ${err}`};
      });
      channel.assertQueue(config.rabbitmq.queue, { durable: false });
      channel.on('error', (err) => {
        throw {err: `[AMQP] Error connected channel: ${err}`};
      });
      let message = `Код подтверждения: ${this.key}`
      let phone = this.phone;
      let buffer = Buffer.from(JSON.stringify({message, phone}).toString('utf8'));
      channel.sendToQueue(config.rabbitmq.queue, buffer);
      channel.on('error', (err) => {
        throw {err: `[AMQP] Error send message: ${err}`};
      });
      setTimeout(function() {
        conn.close();
      }, 500);
    } catch (err) {
      throw err;
      return false;
    }
    return true;
  }
}

module.exports = WorkerAmqp;