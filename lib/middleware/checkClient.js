'use strict'

const ERROR = require('../../const/error.js');

/**
 * @typedef {import("koa").Context} Context
 */

/**
 * Handles HTTP request
 * @param {Context} ctx
 * @param {function} next
 */
async function checkClient(ctx, next) {
    if (ctx.client && typeof ctx.client == 'object' && ctx.client.id) {
        await next();
    }
    else {
        ctx.body = {error: ERROR.PAGES.NOT_CLIENT};
    }
}

module.exports = checkClient;
