'use strict'

const Pino = require('pino');

const logger = Pino({
    timestamp: () => {
        return `,"time":"${(new Date()).toLocaleString()}"`;
    }
});

if (process.env.NODE_ENV == 'test') {
    logger.info = () => { };
}

module.exports = logger;
