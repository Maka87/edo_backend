'use strict'

module.exports = {
  STATUS: {
    EXPECTATION: 1,
    EXECUTION: 2,
    DONE: 3,
    CANCELED: 4
  }
};
