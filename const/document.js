'use strict'

module.exports = {
  DOCUMENTS: {
    1: 'Паспорт',
    2: 'Вид на жительство',
    3: 'Не резидент РФ',
  },
  DOC: {
    FOLDER: {
      EDO: 2110
    }
  }
};
