'use strict'

module.exports = {
    OK: {
        CODE: 200,
        MESSAGE: 'OK'
    },
    NO_CONTENT: {
        CODE: 204,
        MESSAGE: 'No content'
    },
    BAD_REQUEST: {
        CODE: 400,
        MESSAGE: 'Bad request'
    },
    INVALID_INPUT_DATA: {
        CODE: 400,
        MESSAGE: 'Invalid input data'
    },
    UNAUTHORIZED: {
        CODE: 401,
        MESSAGE: 'Unauthorized'
    },
    FORBIDDEN: {
        CODE: 403,
        MESSAGE: 'Forbidden'
    },
    NOT_FOUND: {
        CODE: 404,
        MESSAGE: 'Not found'
    },
    INTERNAL_SERVER_ERROR: {
        CODE: 500,
        MESSAGE: 'Internal server error'
    }
};
