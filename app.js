'use strict'

const config = require('config').util.toObject();
const fs = require('fs').promises;
const http = require('http');
const https = require('https');
const Koa = require('koa');
const bodyparser = require('koa-bodyparser');

const logger = require('./lib/logger.js');
const { router } = require('./lib/router');
const { database, syncPromise } = require('./lib/database/shared-instance.js');
const getClient = require('./lib/middleware/getClient.js');

// const RequestLoggerCH = require('./lib/middleware/request-logger.js');
// const serveStaticFile = require('./lib/middleware/serve-static-file.js');

async function init() {
    await syncPromise;

   // const requestLogger = new RequestLoggerCH(config.requestLoggerCH);

    const app = new Koa();

    app.use(bodyparser({
        jsonLimit: '20mb',
        formLimit: '20mb'
    }));

//     app.use(requestLogger.middleware());

    app.use(getClient);

//     app.use(serveStaticFile);

    app.use(router.routes()).use(router.allowedMethods());

    let httpServer = http.createServer(app.callback());
    httpServer.listen(config.server.port);
    logger.info(`HTTP server is listening on port ${config.server.port}`);

    let httpsServer = null;

    if (config.server.SSL && config.server.SSL.cert && config.server.SSL.key) {
        const SSL = {
            cert: await fs.readFile(config.server.SSL.cert),
            key: await fs.readFile(config.server.SSL.key)
        };
        httpsServer = https.createServer(SSL, app.callback()).listen(config.server.securePort);
        logger.info(`HTTPS server is listening on port ${config.server.securePort}`);
    }

    return {
        httpServer,
        httpsServer,
        database
    };
}

module.exports = init();
