echo ${CI_REGISTRY_PASSWORD} | sudo docker login -u ${CI_REGISTRY_USER} --password-stdin ${CI_REGISTRY}
sudo docker-compose -f docker-compose_edo_backend.yml pull edo_backend
sudo docker-compose -f docker-compose_edo_backend.yml stop edo_backend
sudo docker-compose -f docker-compose_edo_backend.yml up -d --build edo_backend
